# ser-presente

> Site Projeto Ser Presente 

O projeto ser presente distribui sorrisos que chegam em forma de ovos de páscoa. Ao confeccionar os ovos, atingimos um custo unitário muito inferior ao valor comercial dos ovos de páscoa, sendo possível atender mais crianças. Com o trabalho, também se perpetuam nas vidas dos voluntários valores de amor, respeito e dignidade.

## Build Setup

``` bash
# install dependencies
$ yarn install

# serve with hot reload at localhost:3000
$ yarn run dev

# build for production and launch server
$ yarn run build
$ yarn start

# generate static project
$ yarn run generate
```

For detailed explanation on how things work, checkout [Nuxt.js docs](https://nuxtjs.org).
